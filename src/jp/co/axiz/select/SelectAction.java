package jp.co.axiz.select;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.axiz.db.UserDao;
import jp.co.axiz.entity.User;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SelectAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		SelectFrom selectFrom = (SelectFrom)form;
		String next = "ok";
		String msg = "";
		UserDao dao = new UserDao();

		if(selectFrom.getId().equals("")){
			//�S����
			ArrayList<User> userlist = dao.findAll();
			if(userlist.size() != 0){
				request.setAttribute("userlist", userlist);
			}else{
				request.setAttribute("msg", "ID�͂���܂���");
				next = "ng";
			}
		}else{
			//��������
			try{
				User user = dao.findById(Integer.parseInt(selectFrom.getId()));
				request.setAttribute("user", user);
			}catch(NumberFormatException e){
				request.setAttribute("msg", "ID�ɂ͐������͂��Ă�������");
				next = "ng";
			}
		}

		return mapping.findForward(next);
	}
}
