package jp.co.axiz.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * �f�[�^�x�[�X�R�l�N�V�������ȈՓI�Ɉ������[�e�B���e�B�[���\�b�h��񋟂��܂��B
 *
 * @author t.hiraoka
 *
 */
public class DBUtil {
	private static final String DRIVER_NAME = "org.postgresql.Driver";
	private static final String DB_URL = "jdbc:postgresql:axizdb";
	private static final String ID = "axizuser";
	private static final String PASS = "axiz";

	/**
	 * �f�[�^�x�[�X�ւ̃R�l�N�V�������擾���܂��B
	 *
	 * @return DB�ւ̃R�l�N�V����
	 * @throws ClassNotFoundException
	 *             JDBC�h���C�o��������Ȃ������ꍇ
	 * @throws SQLException
	 *             �R�l�N�V�������擾�ł��Ȃ������ꍇ
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName(DRIVER_NAME);
		return DriverManager.getConnection(DB_URL, ID, PASS);
	}

	/**
	 * �J���Ă���DB�R�l�N�V��������܂��B
	 *
	 * @param con
	 *            �R�l�N�V����
	 * @throws SQLException
	 */
	public static void close(Connection con) throws SQLException {
		if (con != null) {
			con.close();
		}
	}

	/**
	 * �J���Ă���X�e�[�g�����g����܂��B
	 *
	 * @param stmt
	 *            �X�e�[�g�����g
	 * @throws SQLException
	 */
	public static void close(Statement stmt) throws SQLException {
		if (stmt != null) {
			stmt.close();
		}
	}

	/**
	 * �C���X�^���X���̋֎~�B
	 *
	 */
	private DBUtil() {
	}

}
