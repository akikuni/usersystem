package jp.co.axiz.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.axiz.entity.User;


/**
 * User�X�g���[�W�փA�N�Z�X���邽�߂�DAO�C���^�[�t�F�[�X�ł��B
 *
 * @author t.hiraoka
 *
 */
public class UserDao {
	final String SEARCH_ALL_SQL = "select * from user_info";
	final String SEARCH_BY_ID_SQL = "select * from user_info where id = ?";
	final String INSERT_USERTABLE_SQL = "insert into user_info (name,tel,id,pass)values(?,?,?,?)";
	final String LASTVAL_SQL = "select lastval()";
	final String UPDATE_USERTABLE_SQL = "update user_info set name = ?, tel = ?, pass = ?,  where id = ?";
	final String DELETE_USERTABLE_SQL = "delete from user_info where id = ?";

	Connection con;
	PreparedStatement stmt;

	public ArrayList<User> findAll() {
		ArrayList<User> list = new ArrayList<User>();;
		try{
			try{
				Connection con = DBUtil.getConnection();
				ResultSet rs = null;

				String sql = SEARCH_ALL_SQL;
				stmt = con.prepareStatement(sql);

				rs = stmt.executeQuery();

				User user = null;

				while (rs.next()) {
					user = new User();
					user.setId(rs.getInt("id"));
					user.setName(rs.getString("name"));
					user.setTel(rs.getString("tel"));
					user.setPass(rs.getString("pass"));

					list.add(user);
				}

				}catch(Exception e){

				}finally{
					if(stmt != null){
						stmt.close();
					}
					if(con != null){
						con.close();
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		return list;
	}

	//�����������s�����\�b�h
	public User findById(int id) {
		User user = null;
		try{
			try{
				Connection con = DBUtil.getConnection();
				ResultSet rs = null;

				String sql = SEARCH_BY_ID_SQL;

				stmt = con.prepareStatement(sql);
				stmt.setInt(1, id);

				rs = stmt.executeQuery();

				if(rs.next()) {
					user = new User();
					user.setId(rs.getInt("id"));
					user.setName(rs.getString("name"));
					user.setTel(rs.getString("tel"));
					user.setPass(rs.getString("pass"));
				}

			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(stmt != null){
					stmt.close();
				}
				if(con != null){
					con.close();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return user;
	}

	public int insert(String name,String tel,String pass) {
		try{
			try{
				Connection con = DBUtil.getConnection();
				int id = getLastUserId(con);

				stmt = con.prepareStatement(INSERT_USERTABLE_SQL);
				stmt.setString(1, name);
				stmt.setString(2, tel);
				stmt.setInt(3, id);
				stmt.setString(4, pass);
				int updateCount = stmt.executeUpdate();
				System.out.println(updateCount);


			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(stmt != null){
					stmt.close();
				}
				if(con != null){
					con.close();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	return 0;
	}

	//�X�V���s�����\�b�h
	public int update(int id,String name,String tel,String pass) {
		try{
			try{
				Connection con = DBUtil.getConnection();

				stmt = con.prepareStatement(UPDATE_USERTABLE_SQL);
				stmt.setInt(3, id);
				stmt.setString(1, name);
				stmt.setString(2, tel);
				int updateCount = stmt.executeUpdate();
				System.out.println(updateCount);

				stmt = con.prepareStatement(UPDATE_USERTABLE_SQL);
				stmt.setInt(2, id);
				stmt.setString(1, pass);
				updateCount = stmt.executeUpdate();
				System.out.println(updateCount);

			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(stmt != null){
					stmt.close();
				}
				if(con != null){
					con.close();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public int delete(int id) {
		try{
			try{
				Connection con = DBUtil.getConnection();

				stmt = con.prepareStatement(DELETE_USERTABLE_SQL);
				stmt.setInt(1, id);
				int updateCount = stmt.executeUpdate();
				System.out.println(updateCount);

			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(stmt != null){
					stmt.close();
				}
				if(con != null){
					con.close();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public int getLastUserId(Connection con) {
		int id = 0;
		try{
			/* INSERT�����s��̎����єԂ��ꂽ�l���擾 */
	        PreparedStatement stmt = con.prepareStatement(LASTVAL_SQL);

	        ResultSet rs = stmt.executeQuery();

			if(rs.next()){
				id = rs.getInt("lastval");
			}
	    	stmt.close();
		}catch(Exception e){
        	e.printStackTrace();
		}
    	return id;
	}

	//UserDaoImpl���ɕێ�����Ă���f�[�^�x�[�X�ڑ�����j��
	public void connectionClose(){
		try {
			DBUtil.close(con);
		} catch (SQLException e) {
			throw new DataAccessException();
		}
	}

	//UserDaoImpl���ɕێ�����Ă���SQL���s�p�C���X�^���X��j��
	private void StatementClose(){
		try {
			DBUtil.close(stmt);
		} catch (SQLException e) {
			throw new DataAccessException();
		}
	}

	//�R���X�g���N�^�ɂăf�[�^�x�[�X�ڑ������擾
	public UserDao() throws ClassNotFoundException, SQLException {
		con = DBUtil.getConnection();
	}
}
