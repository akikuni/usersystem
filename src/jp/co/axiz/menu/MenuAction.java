package jp.co.axiz.menu;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MenuAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		MenuForm menuForm = (MenuForm)form;
		String next = "";

		switch(menuForm.getDml()){
		case "select":
			next = "select";
			break;
		case "insert":
			next = "insert";
			break;
		case "update":
			next = "update";
			break;
		case "delete":
			next = "delete";
			break;
		}

		//System.out.println("MenuAction");


		// TODO �����������ꂽ���\�b�h�E�X�^�u
		return mapping.findForward(next);
	}
}
