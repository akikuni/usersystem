package jp.co.axiz.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.axiz.db.UserDao;
import jp.co.axiz.entity.User;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		LoginForm loginForm = (LoginForm)form;

		String next = "ok";

		UserDao dao = new UserDao();

		System.out.println(loginForm.getId());

		try{
			User user = dao.findById(Integer.parseInt(loginForm.getId()));

			if(user.getPass().equals(loginForm.getPass())){
				request.setAttribute("user", user);
			}else{
				next = "ng";
				request.setAttribute("msg", "login error!");
			}
		}catch(NumberFormatException e){
			next = "ng";
			request.setAttribute("msg", "password error!");
		}

		return mapping.findForward(next);
	}
}
