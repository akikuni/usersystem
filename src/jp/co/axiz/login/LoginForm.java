package jp.co.axiz.login;

import org.apache.struts.action.ActionForm;

public class LoginForm extends ActionForm{
	private String id;
	private String pass;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}



}
