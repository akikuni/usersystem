<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<html:form action="menu">
	こんにちは<bean:write name="user" property="name" />さん<br>
	ID:<bean:write name="loginForm" property="id" />
		<br>
	PASS:<bean:write name="loginForm" property="pass" />
		<br>
		<html:select property="dml">
			<option value="select">検索</option>
			<option value="insert">登録</option>
			<option value="update">更新</option>
			<option value="delete">削除</option>
		</html:select>
		<input type="submit" value="実行">
	</html:form>
</body>
</html:html>